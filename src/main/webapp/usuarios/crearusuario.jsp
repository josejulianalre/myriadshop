<%@include file="../pages/cabeceralogin.jsp"%>  
<link href="${pageContext.request.contextPath}/css/crearusuario.css" rel="stylesheet" type="text/css"/>
<form method="post" action="${pageContext.request.contextPath}/usuario?accion=crear">
    <div class="form-group" align="center">
        <image src="${pageContext.request.contextPath}/assets/img/usuarioImagen.png" width="100"/>
        <div class="form-group" align="center">
            <label id="lbInicioSesion">Registro</label>
            <label >Llenen los datos correspondientes para crear un nuevo perfil</label>
        </div> 
    </div>
    <div class="form-group" align="center">
        <label >
            Nombre:  
        </label>
        <input class="form-control" type="text" name="nombre" id="nombre" placeholder="valentina navarro" required>
    </div>
    <div class="form-group" align="center">
        <label >
            Usuario:  
        </label>
        <input class="form-control" type="text" name="usuario" id="usuario" placeholder="example@gmail.com" required>
    </div>
    <div class="form-group" align="center">
        <label >
            Correo:  
        </label>
        <input class="form-control" type="text" name="correo" id="correo" placeholder="example@gmail.com" required>
    </div>
    <div class="form-group" align="center">
        <label>
            contrasenia: 
        </label>
        <input class="form-control" type="password" name="contrasenia" id="contrasenia" placeholder="*******************" required>

    </div>
    <div class="form-group" align="center">
        <label>
            Confirma tu contrasenia: 
        </label>
        <input class="form-control" type="password" name="contrasenia_confirmar" id="contrasenia_confirmar" placeholder="*******************" required>

    </div>

    <div class="form-group" align="center">

    </div>
    <label></label>
    <div clas="form-group" align="center">
        <button class="btn btn-secondary" type="submit" name="submit"> Crear Perfil</button>
    </div>
    <label></label>
    <hr>
    <div clas="form-group" align="center">
        <a href="${pageContext.request.contextPath}/usuarios/loginusuario.jsp">Inicia sesion</a>
    </div>
</form>

<%@include file="../pages/footer.jsp"%>