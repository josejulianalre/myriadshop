<%@include file="../pages/cabecera.jsp" %>    
<section class="page-section portfolio" id="portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Deparmentos</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
<div class="modal-body">
          <form action="${pageContext.request.contextPath}/usuario?accion=modificar"
                method="post" class="was-validated">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">nombre:</label>
            <input type="text" name="nombre" value="${usuario.nombre}" class="form-control" id="nombre" required>
          </div>
            <div class="mb-3">
            <label for="recipient-name" class="col-form-label">usuario:</label>
            <input type="text" name="usuario" value="${usuario.usuario}" class="form-control" id="usuario" required>
          </div>
            
            <div class="mb-3">
            <label for="recipient-name" class="col-form-label">correo</label>
            <input  type="text" name="correo" value="${usuario.correo}" class="form-control" id="correo" required readonly="readonly">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">contrasenia</label>
            <input type="text" name="contrasenia" value="${usuario.contrasenia}" class="form-control" id="contrasenia" required>
          </div>
           <div class="mb-3">
            <label for="recipient-name" class="col-form-label">confirma tu contrasenia</label>
            <input type="text" name="confirma_contrasenia" value="${usuario.contrasenia}" class="form-control" id="confirma_contrasenia" required>
          </div>
          <div class="modal-footer">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/usuario" role="button">Cerrar</a>
            <input type="submit" class="btn btn-primary" value="Editar"/>
            </div>
        </form>
</div>
    </div>
            </<section>
<%@include file="../pages/footer.jsp" %>