<%@include file="../pages/cabecera.jsp"%>   


    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Inicio</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/usuarios/crearusuario.jsp" role="button"><i class="fas fa-plus-square"></i></a>
        <table class="table" style="text-align:center;">
            <thead>
                <tr>
                    <th scope="col" >nombre</th>
                    <th scope="col">correo</th>
                    <th scope="col">usuario</th>
                    <th scope="col">contrasenia</th>
                    <th scope="col">opciones</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="u" items="${us}">
                <tr>
                        <th scope="row">${u.nombre}</th>
                        <td>${u.correo}</td>
                        <td>${u.usuario}</td>
                        <td>${u.contrasenia}</td>
                        <td><a class="btn btn-primary" href="${pageContext.request.contextPath}/usuario?correo=${u.correo}&accion=eliminar" role="button"><i class="fas fa-trash-alt"></i></a> <a class="btn btn-primary" href="${pageContext.request.contextPath}/usuario?correo=${u.correo}&accion=editar" role="button"><i class="fas fa-pen-square"></i></a></td>  
                </tr>
                </c:forEach>
            </tbody>
        </table>


<%@include file="../pages/footer.jsp"%>
