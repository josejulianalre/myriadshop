/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Web;

import Modelo.UsuarioDAO;
import Modelo.UsuarioDTO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/usuario")
public class UsuarioController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    this.eliminar(req, resp);
                    break;

                case "editar":
                    this.editar(req, resp);
                    break;

                default:
                    this.listarUsuarios(req, resp);
                    break;
            }
        } else {
            this.listarUsuarios(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;

                case "modificar":
                    this.modificar(req, resp);
                    break;

                case "ingresar":
                    this.ingresar(req, resp);
                    break;

            }
        }

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioDAO uDao = new UsuarioDAO();
        String nombre = req.getParameter("nombre");
        String usuario = req.getParameter("usuario");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        if (uDao.consultar_usuario(correo) == null) {
            uDao.crear(new UsuarioDTO(nombre, correo, usuario, contrasenia));
            this.listarUsuarios(req, resp);
        }else{
            req.setAttribute("nombre", nombre);
            req.setAttribute("usuario", usuario);
            req.setAttribute("contrasenia", contrasenia);
            req.setAttribute("contrasenia_confirmar", contrasenia);
            req.setAttribute("ERROR", "Este correo ya esta registrado, intenta con otro");
            req.getRequestDispatcher("usuarios/crearusuario.jsp").forward(req, resp);
        }
    }

    private void listarUsuarios(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioDAO uDao = new UsuarioDAO();
        List<UsuarioDTO> usuarios = uDao.consultar();
        req.setAttribute("us", usuarios);
        req.getRequestDispatcher("usuarios/verusuario.jsp").forward(req, resp);
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioDAO uDao = new UsuarioDAO();
        String correo = req.getParameter("correo");
        uDao.eliminar(new UsuarioDTO(correo));
        this.listarUsuarios(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioDAO uDao = new UsuarioDAO();
        String correo = req.getParameter("correo");
        UsuarioDTO usuario = uDao.consultar_usuario(correo);
        req.setAttribute("usuario", usuario);
        req.getRequestDispatcher("usuarios/editarusuario.jsp").forward(req, resp);
    }

    private void modificar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioDAO uDao = new UsuarioDAO();
        String nombre = req.getParameter("nombre");
        String usuario = req.getParameter("usuario");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        uDao.actualizar(new UsuarioDTO(nombre, correo, usuario, contrasenia));
        this.listarUsuarios(req, resp);

    }

    private void ingresar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioDAO uDao = new UsuarioDAO();
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        UsuarioDTO u = uDao.consultar_usuario(correo);
        if (u != null) {

        } else {
            req.getRequestDispatcher("usuarios/loginusuario.jsp").forward(req, resp);
        }

    }

}
