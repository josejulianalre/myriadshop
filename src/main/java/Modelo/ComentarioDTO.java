/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author ACER
 */
public class ComentarioDTO {

    private int id;
    private String correo_usuario;
    private String nit_tienda;
    private Date fecha;
    private int puntuacion;
    private String comentario;

    public ComentarioDTO(int id, String correo_usuario, String nit_tienda, Date fecha, int puntuacion, String comentario) {
        this.id = id;
        this.correo_usuario = correo_usuario;
        this.nit_tienda = nit_tienda;
        this.fecha = fecha;
        this.puntuacion = puntuacion;
        this.comentario = comentario;
    }

    public ComentarioDTO() {
    }

    public String getCorreo_usuario() {
        return correo_usuario;
    }

    public void setCorreo_usuario(String correo_usuario) {
        this.correo_usuario = correo_usuario;
    }

    public String getNit_tienda() {
        return nit_tienda;
    }

    public void setNit_tienda(String nit_tienda) {
        this.nit_tienda = nit_tienda;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
    
    

    @Override
    public String toString() {
        return "ComentarioDTO '" + id + "'" + "\n" + "correo_usuario = " + correo_usuario + "\n" + "nit_tienda = " + nit_tienda + "\n" + "fecha = " + fecha + "\n" + "puntuacion = " + puntuacion + "\n" + "comentario = " + comentario + "\n" + "\n";
    }

}
