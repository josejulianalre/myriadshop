/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import static Modelo.TiendaDAO.SQL_CONSULTA;
import static Modelo.TiendaDAO.SQL_DELETE;
import static Modelo.TiendaDAO.SQL_INSERT;
import static Modelo.TiendaDAO.SQL_UPDATE;
import Negocio.DBC;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class ComentarioDAO implements ComentarioServices {

    public static final String SQL_CONSULTA = "SELECT id,correo_usuario, nit_tienda, fecha, puntuacion, comentario FROM comentarios";
    public static final String SQL_CONSULTA_POR_USUARIO = "SELECT id,correo_usuario, nit_tienda, fecha, puntuacion, comentario FROM comentarios WHERE correo_usuario = ?";
    public static final String SQL_CONSULTA_POR_TIENDA = "SELECT id,correo_usuario, nit_tienda, fecha, puntuacion, comentario FROM comentarios WHERE nit_tienda = ?";
    public static final String SQL_CONSULTA_COMENTARIO = "SELECT id,correo_usuario, nit_tienda, fecha, puntuacion, comentario FROM comentarios WHERE id = ?";
    public static final String SQL_INSERT = "INSERT INTO comentarios (correo_usuario, nit_tienda, fecha, puntuacion, comentario) VALUES (?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM comentarios WHERE correo_usuario = ? AND nit_tienda AND fecha = ? AND puntuacion = ? AND comentario = ?";
    public static final String SQL_DELETE_POR_USUARIO = "DELETE FROM comentarios WHERE correo_usuario = ?";
    public static final String SQL_DELETE_POR_TIENDA = "DELETE FROM comentarios WHERE nit_tienda = ?";
    public static final String SQL_UPDATE = "UPDATE comentarios SET  puntuacion = ?, comentario = ? WHERE correo_usuario = ? AND nit_tienda = ? AND fecha = ?";

    @Override
    public List<ComentarioDTO> consultar() {
        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        ComentarioDTO c = null;
        List<ComentarioDTO> comentarios = new ArrayList<>();
        try {
            con = DBC.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String correo_usuario = res.getString("correo_usuario");
                String nit_tienda = res.getString("nit_tienda");
                Date fecha = res.getDate("fecha");
                int puntuacion = res.getInt("puntuacion");
                String comentario = res.getString("comentario");

                c = new ComentarioDTO();
                comentarios.add(new ComentarioDTO(id, correo_usuario, nit_tienda, fecha, puntuacion, comentario));
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                DBC.close(res);
                DBC.close(sen);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return comentarios;
    }

    @Override
    public List<ComentarioDTO> consultar(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        ComentarioDTO c = null;
        List<ComentarioDTO> comentarios = new ArrayList();
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_POR_USUARIO, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setString(1, usuarioDTO.getCorreo());
            set = pstm.executeQuery();
            while (set.next()) {
                int id = set.getInt("id");
                String correo_usuario = set.getString("correo_usuario");
                String nit_tienda = set.getString("nit_tienda");
                Date fecha = set.getDate("fecha");
                int puntuacion = set.getInt("puntuacion");
                String comentario = set.getString("comentario");
                c = new ComentarioDTO(id, correo_usuario, nit_tienda, fecha, puntuacion, comentario);
                comentarios.add(c);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return comentarios;
    }

    @Override
    public List<ComentarioDTO> consultar(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        ComentarioDTO c = null;
        List<ComentarioDTO> comentarios = new ArrayList();

        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_POR_TIENDA, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setString(1, tiendaDTO.getNit());
            set = pstm.executeQuery();
            while (set.next()) {
                int id = set.getInt("id");
                String correo_usuario = set.getString("correo_usuario");
                String nit_tienda = set.getString("nit_tienda");
                Date fecha = set.getDate("fecha");
                int puntuacion = set.getInt("puntuacion");
                String comentario = set.getString("comentario");
                c = new ComentarioDTO(id, correo_usuario, nit_tienda, fecha, puntuacion, comentario);
                comentarios.add(c);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return comentarios;
    }

    @Override
    public int crear(ComentarioDTO comentarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_INSERT);
            pstm.setString(1, comentarioDTO.getCorreo_usuario());
            pstm.setString(2, comentarioDTO.getNit_tienda());
            pstm.setDate(3, comentarioDTO.getFecha());
            pstm.setInt(4, comentarioDTO.getPuntuacion());
            pstm.setString(5, comentarioDTO.getComentario());
            registros = pstm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public ComentarioDTO consultar(int id_comentario) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        ComentarioDTO c = null;

        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_COMENTARIO, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setInt(1, id_comentario);
            set = pstm.executeQuery();
            while (set.next()) {
                int id = set.getInt("id");
                String correo_usuario = set.getString("correo_usuario");
                String nit_tienda = set.getString("nit_tienda");
                Date fecha = set.getDate("fecha");
                int puntuacion = set.getInt("puntuacion");
                String comentario = set.getString("comentario");
                c = new ComentarioDTO(id, correo_usuario, nit_tienda, fecha, puntuacion, comentario);
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return c;
    }

    @Override
    public int actualizar(ComentarioDTO comentarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_UPDATE);

            pstm.setInt(1, comentarioDTO.getPuntuacion());
            pstm.setString(2, comentarioDTO.getComentario());
            pstm.setString(3, comentarioDTO.getCorreo_usuario());
            pstm.setString(4, comentarioDTO.getNit_tienda());
            pstm.setDate(5, comentarioDTO.getFecha());

            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(ComentarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ComentarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar(ComentarioDTO comentarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {

            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE);
            pstm.setString(1, comentarioDTO.getCorreo_usuario());
            pstm.setString(2, comentarioDTO.getNit_tienda());
            pstm.setDate(3, comentarioDTO.getFecha());
            pstm.setInt(4, comentarioDTO.getPuntuacion());
            pstm.setString(5, comentarioDTO.getComentario());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {

            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE_POR_USUARIO);
            pstm.setString(1, usuarioDTO.getCorreo());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {

            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE_POR_TIENDA);
            pstm.setString(1, tiendaDTO.getNit());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

}
