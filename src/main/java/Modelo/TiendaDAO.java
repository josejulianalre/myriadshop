/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import static Modelo.UsuarioDAO.SQL_CONSULTA;
import static Modelo.UsuarioDAO.SQL_CONSULTA_TIENDAS_FAVS;
import static Modelo.UsuarioDAO.SQL_INSERT;
import static Modelo.UsuarioDAO.SQL_UPDATE;
import Negocio.DBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class TiendaDAO implements TiendaServices {

    public static final String SQL_CONSULTA = "SELECT nit,link,correo,usuario,nombre,contrasenia,horario,direccion,telefono,visitas_mensuales FROM tienda";
    public static final String SQL_CONSULTA_NIT = "SELECT nit,link,correo,usuario,nombre,contrasenia,horario,direccion,telefono,visitas_mensuales FROM tienda WHERE nit = ?";
    public static final String SQL_CONSULTA_SEGUIDORES = "SELECT correo_usuario FROM tiendas_favoritas WHERE nit_tienda = ?";
    public static final String SQL_INSERT = "INSERT INTO tienda (nit,link,correo,usuario,nombre,contrasenia,horario,direccion,telefono,visitas_mensuales) VALUES (?,?,?,?,?,?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM tienda WHERE nit = ?";
    public static final String SQL_DELETE_SEGUIDORES = "DELETE FROM tiendas_favoritas WHERE nit_tienda = ?";
    public static final String SQL_DELETE_SEGUIDOR = "DELETE FROM tiendas_favoritas WHERE nit_tienda = ? AND correo_usuario = ?";

    public static final String SQL_UPDATE = "UPDATE tienda SET link = ?, correo = ?, usuario = ?, nombre = ?,contrasenia = ?, horario = ?, direccion = ?,telefono = ?, visitas_mensuales = ? WHERE nit = ?";

    @Override
    public List<TiendaDTO> consultar() {
        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        TiendaDTO t = null;
        List<TiendaDTO> tiendas = new ArrayList<>();
        try {
            con = DBC.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();
            while (res.next()) {
                String nit = res.getString("nit");
                String link = res.getString("link");
                String correo = res.getString("correo");
                String usuario = res.getString("usuario");
                String nombre = res.getString("nombre");
                String contrasenia = res.getString("contrasenia");
                String horario = res.getString("horario");
                String direccion = res.getString("direccion");
                String telefono = res.getString("telefono");
                int num_visitas = res.getInt("visitas_mensuales");

                t = new TiendaDTO(nit, link, correo, usuario, nombre, contrasenia, horario, direccion, telefono, num_visitas);
                tiendas.add(t);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                DBC.close(res);
                DBC.close(sen);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return tiendas;
    }

    @Override
    public int crear(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_INSERT);
            pstm.setString(1, tiendaDTO.getNit());
            pstm.setString(2, tiendaDTO.getLink());
            pstm.setString(3, tiendaDTO.getCorreo());
            pstm.setString(4, tiendaDTO.getUsuario());
            pstm.setString(5, tiendaDTO.getNombre());
            pstm.setString(6, tiendaDTO.getContrasenia());
            pstm.setString(7, tiendaDTO.getHorario());
            pstm.setString(8, tiendaDTO.getDireccion());
            pstm.setString(9, tiendaDTO.getTelefono());
            pstm.setInt(10, tiendaDTO.getNum_visitas());
            registros = pstm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_UPDATE);

            pstm.setString(1, tiendaDTO.getLink());
            pstm.setString(2, tiendaDTO.getCorreo());
            pstm.setString(3, tiendaDTO.getUsuario());
            pstm.setString(4, tiendaDTO.getNombre());
            pstm.setString(5, tiendaDTO.getContrasenia());
            pstm.setString(6, tiendaDTO.getHorario());
            pstm.setString(7, tiendaDTO.getDireccion());
            pstm.setString(8, tiendaDTO.getTelefono());
            pstm.setInt(9, tiendaDTO.getNum_visitas());
            pstm.setString(10, tiendaDTO.getNit());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(TiendaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {

            ComentarioDAO c = new ComentarioDAO();
            c.eliminar(tiendaDTO);
            this.eliminar_Seguidores(tiendaDTO);
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE);
            pstm.setString(1, tiendaDTO.getNit());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public TiendaDTO consultar_tienda(String nit) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        TiendaDTO t = null;
        List<TiendaDTO> tiendas = new ArrayList();

        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_NIT, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setString(1, nit);
            set = pstm.executeQuery();

            while (set.next()) {
                String nit_tienda = set.getString("nit");
                String link = set.getString("link");
                String correo = set.getString("correo");
                String usuario = set.getString("usuario");
                String nombre = set.getString("nombre");
                String contrasenia = set.getString("contrasenia");
                String horario = set.getString("horario");
                String direccion = set.getString("direccion");
                String telefono = set.getString("telefono");
                int num_visitas = set.getInt("visitas_mensuales");

                t = new TiendaDTO(nit, link, correo, usuario, nombre, contrasenia, horario, direccion, telefono, num_visitas);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return t;
    }

    @Override
    public List<UsuarioDTO> consultar_seguidores(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        UsuarioDTO u = null;
        List<UsuarioDTO> usuarios = new ArrayList();
        UsuarioDAO us = new UsuarioDAO();

        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_SEGUIDORES, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setString(1, tiendaDTO.getNit());
            set = pstm.executeQuery();

            while (set.next()) {
                String correo_usuario = set.getString("correo_usuario");
                u = us.consultar_usuario(correo_usuario);
                usuarios.add(u);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return usuarios;
    }

    @Override
    public int eliminar_Seguidores(TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE_SEGUIDORES);
            pstm.setString(1, tiendaDTO.getNit());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar_Seguidor(TiendaDTO tiendaDTO, UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE_SEGUIDOR);
            pstm.setString(1, tiendaDTO.getNit());
            pstm.setString(2,usuarioDTO.getCorreo());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

}
