/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;

/**
 *
 * @author ACER
 */
public interface UsuarioServices {

    /*
        Consulta todos los usuarios registrados en la aplicacion
     */
    public List<UsuarioDTO> consultar();

    /*
       Consulta el listado de tiendas favoritas de un usuario en especifico
     */
    public List<TiendaDTO> consultar_tiendas_favoritas(UsuarioDTO usuarioDTO);

    /*
        Consulta la informacion de un usuario en especifico
    */
    public UsuarioDTO consultar_usuario(String usuario);

    /*
        Crea un nuevo usuario en la aplicacion
    */
    public int crear(UsuarioDTO usuarioDTO);

    /*
        Actaliza el perfil de un usuario registrado anteriormente
    */
    public int actualizar(UsuarioDTO usuarioDTO);
    
    /*
        Elimina el perfil de un usuario
    */
    public int eliminar(UsuarioDTO usuarioDTO);
    
    /*
        Elimina todas las tiendas favoritas de un usuario
    */
    public int eliminar_favs(UsuarioDTO usuarioDTO);
    
    /*
        Elimina una tienda especifica de los favoritos de un usuario
    */
    public int eliminar_fav(UsuarioDTO usuarioDTO,TiendaDTO tiendaDTO);

}
