/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author ACER
 */
public class UsuarioDTO {

    private String nombre;
    private String correo;
    private String usuario;
    private String contrasenia;
    private List<TiendaDTO> tiendas_favoritas;

    public UsuarioDTO(String nombre, String correo, String usuario, String contrasenia, List<TiendaDTO> tiendas_favoritas) {
        this.nombre = nombre;
        this.correo = correo;
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.tiendas_favoritas = tiendas_favoritas;
    }

    public UsuarioDTO(String nombre, String correo, String usuario, String contrasenia) {
        this.nombre = nombre;
        this.correo = correo;
        this.usuario = usuario;
        this.contrasenia = contrasenia;

    }

    public UsuarioDTO() {
    }

    public UsuarioDTO(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioDTO other = (UsuarioDTO) obj;
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        return true;
    }

    public List<TiendaDTO> getTiendas_favoritas() {
        return tiendas_favoritas;
    }

    public void setTiendas_favoritas(List<TiendaDTO> tiendas_favoritas) {
        this.tiendas_favoritas = tiendas_favoritas;
    }

    @Override
    public String toString() {
        return "Usuario '" + correo + "': " + "\n" + "nombre = " + nombre + "\n" + "correo = " + correo + "\n" + "usuario = " + usuario + "\n" + "contrasenia = " + contrasenia + "\n" + "Tiendas favoritas = " + tiendas_favoritas.toString() + "\n";
    }

}
