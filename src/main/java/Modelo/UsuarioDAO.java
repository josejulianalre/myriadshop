/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import static Modelo.ComentarioDAO.SQL_CONSULTA;
import static Modelo.TiendaDAO.SQL_DELETE_SEGUIDOR;
import static Modelo.TiendaDAO.SQL_DELETE_SEGUIDORES;
import Negocio.DBC;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class UsuarioDAO implements UsuarioServices {

    public static final String SQL_CONSULTA = "SELECT nombre,correo,usuario,contrasenia FROM usuarios";
    public static final String SQL_CONSULTA_TIENDAS_FAVS = "SELECT nit_tienda FROM tiendas_favoritas WHERE correo_usuario = ?";
    public static final String SQL_CONSULTA_USUARIO = "SELECT nombre,correo,usuario,contrasenia FROM usuarios WHERE correo = ?";
    public static final String SQL_INSERT = "INSERT INTO usuarios (nombre,correo,usuario,contrasenia) VALUES (?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM usuarios WHERE correo = ?";
    public static final String SQL_UPDATE = "UPDATE usuarios SET nombre = ?, usuario = ?, contrasenia = ? WHERE correo = ?";
    public static final String SQL_DELETE_FAVS = "DELETE FROM tiendas_favoritas WHERE correo_usuario = ?";
    public static final String SQL_DELETE_SEGUIDOR = "DELETE FROM tiendas_favoritas WHERE correo_usuario = ? AND nit_tienda = ?";

    @Override
    public List<UsuarioDTO> consultar() {
        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        UsuarioDTO u = null;
        List<UsuarioDTO> usuarios = new ArrayList<>();
        List<TiendaDTO> tiendas = new ArrayList();
        try {
            con = DBC.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();
            while (res.next()) {
                String nombre = res.getString("nombre");
                String correo = res.getString("correo");
                String usuario = res.getString("usuario");
                String contrasenia = res.getString("contrasenia");
                u = new UsuarioDTO(nombre, correo, usuario, contrasenia);
                u.setTiendas_favoritas(this.consultar_tiendas_favoritas(u));
                usuarios.add(u);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                DBC.close(res);
                DBC.close(sen);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return usuarios;
    }

    @Override
    public int crear(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_INSERT);
            pstm.setString(1, usuarioDTO.getNombre());
            pstm.setString(2, usuarioDTO.getCorreo());
            pstm.setString(3, usuarioDTO.getUsuario());
            pstm.setString(4, usuarioDTO.getContrasenia());
            registros = pstm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_UPDATE);

            pstm.setString(1, usuarioDTO.getNombre());
            pstm.setString(2, usuarioDTO.getUsuario());
            pstm.setString(3, usuarioDTO.getContrasenia());
            pstm.setString(4, usuarioDTO.getCorreo());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            ComentarioDAO c = new ComentarioDAO();
            c.eliminar(usuarioDTO);
            this.eliminar_favs(usuarioDTO);
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE);
            pstm.setString(1, usuarioDTO.getCorreo());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public List<TiendaDTO> consultar_tiendas_favoritas(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        TiendaDTO t = null;
        List<TiendaDTO> tiendas = new ArrayList();

        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_TIENDAS_FAVS, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setString(1, usuarioDTO.getCorreo());
            set = pstm.executeQuery();

            while (set.next()) {
                String nit_tienda = set.getString("nit_tienda");
                TiendaDAO d = new TiendaDAO();
                t = d.consultar_tienda(nit_tienda);
                tiendas.add(t);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return tiendas;
    }

    @Override
    public UsuarioDTO consultar_usuario(String correo_usuario) {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet set = null;
        UsuarioDTO u = null;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_CONSULTA_USUARIO, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pstm.setString(1, correo_usuario);
            set = pstm.executeQuery();
            while (set.next()) {
                String nombre = set.getString("nombre");
                String correo = set.getString("correo");
                String usuario = set.getString("usuario");
                String contrasenia = set.getString("contrasenia");
                u = new UsuarioDTO(nombre, correo, usuario, contrasenia);
                u.setTiendas_favoritas(this.consultar_tiendas_favoritas(u));
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                //Se cierra al reves para evitar que queden logs en el proceso
                DBC.close(pstm);
                DBC.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return u;
    }

    @Override
    public int eliminar_favs(UsuarioDTO usuarioDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE_FAVS);
            pstm.setString(1, usuarioDTO.getCorreo());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int eliminar_fav(UsuarioDTO usuarioDTO, TiendaDTO tiendaDTO) {
        Connection con = null;
        PreparedStatement pstm = null;
        int registros = 0;
        try {
            con = DBC.getConnection();
            pstm = con.prepareStatement(SQL_DELETE_SEGUIDOR);
            pstm.setString(1, usuarioDTO.getCorreo());
            pstm.setString(2, tiendaDTO.getNit());
            registros = pstm.executeUpdate();

            return registros;
        } catch (SQLException ex) {
            Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

}
