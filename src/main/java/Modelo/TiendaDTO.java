/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author ACER
 */
public class TiendaDTO {

    private String nit;
    private String link;
    private String correo;
    private String usuario;
    private String nombre;
    private String contrasenia;
    private String horario;
    private String direccion;
    private String telefono;
    List<UsuarioDTO> seguidores;
    private int num_visitas;

    public TiendaDTO(String nit, String link, String correo, String usuario, String nombre, String contrasenia, String horario, String direccion,String telefono, int num_visitas,List<UsuarioDTO> seguidores) {
        this.nit = nit;
        this.link = link;
        this.correo = correo;
        this.usuario = usuario;
        this.nombre = nombre;
        this.contrasenia = contrasenia;
        this.horario = horario;
        this.direccion = direccion;
        this.telefono = telefono;
        this.num_visitas = num_visitas;
        this.seguidores = seguidores;
    }
    
    public TiendaDTO(String nit, String link, String correo, String usuario, String nombre, String contrasenia, String horario, String direccion,String telefono, int num_visitas) {
        this.nit = nit;
        this.link = link;
        this.correo = correo;
        this.usuario = usuario;
        this.nombre = nombre;
        this.contrasenia = contrasenia;
        this.horario = horario;
        this.direccion = direccion;
        this.telefono = telefono;
        this.num_visitas = num_visitas;
    }

    public TiendaDTO() {
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNum_visitas() {
        return num_visitas;
    }

    public void setNum_visitas(int num_visitas) {
        this.num_visitas = num_visitas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TiendaDTO other = (TiendaDTO) obj;
        if (!Objects.equals(this.nit, other.nit)) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TiendaDTO '" + nit + "':" + "\n" + "nit = " + nit + "\n" + "link = " + link + "\n" + "correo = " + correo + "\n" + "usuario = " + usuario + "\n" + "nombre = " + nombre + "\n" + "contrasenia = " + contrasenia + "\n" + "horario = " + horario + "\n" + "direccion = " + direccion + "\n" +"telefono = " + telefono + "\n" + "num_visitas = " + num_visitas + "\n" + "\n";
    }
    
    

}
