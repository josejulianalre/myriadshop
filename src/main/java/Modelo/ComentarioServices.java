/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;

/**
 *
 * @author ACER
 */
public interface ComentarioServices {

    /*
        Consulta todos los comentarios de la aplicacion
    */
    public List<ComentarioDTO> consultar();
    
    /*
        Consulta los comentarios de un usuario en especifico
    */
    public List<ComentarioDTO> consultar(UsuarioDTO usuarioDTO);

    /*
        Consulta todos los comentarios ligados a una tienda
    */
    public List<ComentarioDTO> consultar(TiendaDTO tiendaDTO);

    /*
        Consulta un comentario por si id
    */
    public ComentarioDTO consultar(int id_comentario);

    /*
        Crea un nuevo comentario
    */
    public int crear(ComentarioDTO comentarioDTO);

    /*
        Actualiza un comentario hecho
    */
    public int actualizar(ComentarioDTO comentarioDTO);

    /*
        Elimina un comentario en especifico
    */
    public int eliminar(ComentarioDTO comentarioDTO);
    
    /*
        Elimina todos los comentarios de un usuario
    */
    public int eliminar(UsuarioDTO usuarioDTO);
    
    /*
        Elimina todos los comentarios ligados a una tienda
    */
    public int eliminar(TiendaDTO tiendaDTO);

}
