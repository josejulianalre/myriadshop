/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;

/**
 *
 * @author ACER
 */
public interface TiendaServices {

    /*
        Consulta todas las tiendas registradas en la aplicacion
     */
    public List<TiendaDTO> consultar();

    /*
        Consulta los datos de una tienda en especifico
     */
    public TiendaDTO consultar_tienda(String nit);

    /*
        Consulta el listado de seguidores de una tienda
     */
    public List<UsuarioDTO> consultar_seguidores(TiendaDTO tiendaDTO);

    /*
        Crea una nueva tienda en la aplicacion
     */
    public int crear(TiendaDTO tiendaDTO);

    /*
        Actualiza los datos de una tienda
     */
    public int actualizar(TiendaDTO tiendaDTO);

    /*
        Elimina una tienda de la aplicacion
     */
    public int eliminar(TiendaDTO tiendaDTO);

    /*
        Elimina todos los seguidores de una tienda
    */
    public int eliminar_Seguidores(TiendaDTO tiendaDTO);
    
    /*
        Elimina un seguidor especifico de una tienda especifica
    */
    public int eliminar_Seguidor(TiendaDTO tiendaDTO,UsuarioDTO usuarioDTO);

}
